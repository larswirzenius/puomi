# Installing Puomi

Puomi is Internet router software for home and small office use. It
consists of [Debian][] with a custom configuration to enable use of a
PC as a router.

To use Puomi you need:

* A PC with at least two Ethernet ports. Below, we call it "the router
  PC".
  - a USB-Ethernet adapter is fine for the second port
* A USB drive to hold the installer image.
* A second machine to provision the router. Below, we call it the
  "laptop", but it can be any machine.
* Some Ethernet cables.

To install, short version:

* Install Debian on the router PC.
* From another machine, run Ansible with the `puomi` role, to set up
  Puomi on the router PC.
* Reboot the router PC

The router *should* now connect to the Internet via its primary
Ethernet port, and serves local machines on the LAN via its other
Ethernet ports.

The `puomi` role can be configured using the following Ansible
variables:

* `puomi_version`: set this to "1"
* `puomi_hostname`: set this to the host name of the router PC
* `puomi_lan_ip`: the IPv4 address of the LAN interface of router PC
* `puomi_dhcp_start`: the first address for DHCP leases
* `puomi_dhcp_end`: the last address for DHCP leases
* `puomi_dhcp_netmask`: netmask for the DHCP leases (e.g,
  "255.255.255.0")
* `puomi_dhcp_lease`: how long DHCP leases last (e.g., "1h")

Sample values:

~~~yaml
puomi_version: 1
puomi_hostname: "{{ invenstory_hostname }}"
puomi_lan_ip: 10.4.1.1
puomi_dhcp_start: 10.4.1.10
puomi_dhcp_end: 10.4.1.250
puomi_dhcp_netmask: 255.255.255.0
puomi_dhcp_lease: 1h
~~~

FIXME: Provide more detailed instruction that are actually helpful.
