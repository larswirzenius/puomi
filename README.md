# Puomi, a Debian based home or small office Internet router

> Note: The development status of Puomi is that some of the very
> basics work, but it's lacking many features that many people want in
> a router. Help improving Puomi would be very much welcome.

Puomi is a way to install Debian on a computer and configure it to be
an Internet router for home or small office use. A Puomi router
provides:

* routing between an internal and external network
* IPv4 NAT
  - please help add IPv6 support
* DNS to the local network
* DHCP to the local network, with hosts added to local DNS

Puomi is a very basic Debian system, without any user interface. It is
installed onto a PC running Debian, and post-installation
configuration and administration is expected to be done using SSH and
Ansible.

## The goal

The goal of Puomi is to be a home and small office Internet router,
running on a PC, and easy to hack on by its administrator. It's meant
to be configured and upgraded using Ansible. Puomi is, in face,
implemented as an Ansible role.

However, Puomi also aims to require as little attention as possible.

## Architecture

A brief overview of how the current Puomi architecture. This will
change as Puomi evolves.

* Puomi is based on Debian and Linux.
* Puomi is meant to be simple.
* Puomi is only concerned by the networking aspects. It does not take
  responsibility for the system as whole.
* Puomi is meant to either not need care taking, or be adjusted using
  a configuration management system such as Ansible. It has no user
  interface beyond the usual Linux/Debian server tooling.
* Networking is controlled via `systemd-networkd`.
* One Ethernet port, `eth0`, if designated as the connection to the
  Internet. It gets an address using DHCP.
* All other Ethernet ports and wifi interfaces are bridged together,
  so that all machines on the LAN see each other.
* `dnsmasq` provides DHCP and DNS to LAN. Every DHCP client hostname
  is automatically added to the local DNS.
* WiFi access point is provided by `hostapd` for the `wlen0`
  interface.
* `systemd-revolved` managed `/etc/resolv.conf` for the external DNS
  server.


## Installation

See [install.md][].

## Hardware

Puomi is a software project, but of course it needs to run on actual
hardware.

Puomi currently targets a Lenovo Thinkpad X220 laptop with a USB
Ethernet adapter installed. We expect that any reasonably standard PC
will work.

Possible other hardware we may want to try some day:

* NitroWall: https://shop.nitrokey.com/shop?&search=nitrowall
* Protectli: https://eu.protectli.com/product-comparison/
* Orance Pi: <http://www.orangepi.org/orangepiwiki/index.php/Orange_Pi_5_detail>
* Raspberry Pi
* Turris Mox: https://www.turris.com/en/products/omnia/
* Turris Omnia: https://www.turris.com/en/products/mox/

## Related links

- <https://routersecurity.org/consumerrouters.php>  
  Criticisms of consumer routers.

- <https://www.fkie.fraunhofer.de/content/dam/fkie/de/documents/HomeRouter/HomeRouterSecurity_2020_Bericht.pdf>  
  Home Router Security Report 2020, by Frauhofer FKIE
